<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['judul','isi','images'];

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }
    public function tags(){
        return $this->belongsToMany('App\Tag','article_has_tags','article_id','tag_id');
    }
}
