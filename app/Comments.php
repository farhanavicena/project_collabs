<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

    protected $table = 'comments';
    protected $fillable = ['article_id','user_id','isi_komentar'];
    public function articel(){
        return $this->belongsTo(Article::class);
    }
}
