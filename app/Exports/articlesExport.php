<?php

namespace App\Exports;

use App\article;
use Maatwebsite\Excel\Concerns\FromCollection;

class articlesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return article::all();
    }
}
