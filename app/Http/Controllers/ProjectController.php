<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Article;
use App\Tag;

use DB;

class ProjectController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $articles = Article::all();

        return view('project.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('project.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:articles',
            'isi' => 'required',
            'tag' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);


        $tags_arr = explode(',' , $request["tag"]);
        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::where("tag_name", $tag_name)->first();
            if($tag){
                $tag_ids[] = $tag->id;
            } else {
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }


        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('/img/article/'), $imageName);

        $article = Article::create([
                    'judul' => $request['judul'],
                    'isi' => $request['isi'],
                    'images' => $imageName
        ]);



        $article->tags()->sync($tag_ids);

        Alert::success('Success','Artikel Berhasil di Publish');


        return redirect('/project')->with('berhasil', 'Your Article has Published!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        $article = Article::find($id);

        //untuk ambil tagsnya juga
        $info = DB::table('article_has_tags')
                ->join('articles', 'articles.id', '=', 'article_has_tags.article_id')
                ->join('tags', 'tags.id', '=', 'article_has_tags.tag_id')
                ->select('articles.*', 'tags.tag_name')
                ->where('articles.id', $id)
                ->get();
        // dd($info);


        return view('project.show', compact('article', 'info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $article = Article::find($id);
        $info = DB::table('article_has_tags')
                ->join('articles', 'articles.id', '=', 'article_has_tags.article_id')
                ->join('tags', 'tags.id', '=', 'article_has_tags.tag_id')
                ->select('articles.*', 'tags.tag_name')
                ->where('articles.id', $id)
                ->get();

        return view("project.edit", compact('article', 'info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'tag' => 'required'
        ]);

        $tags = DB::table('tags')->where('tag_name', $request->tag)->first();
        if ($tags == null) {
            DB::table('tags')->insert(['tag_name' => $request->tag]);
        }
        $tag = DB::table('tags')->where('tag_name', $request->tag)->first();

        $original = Article::where('id', $id)->first();
        $imageName = $original->images;
        if($request->image!=null){
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('/img/article/'), $imageName);
        }
        $article = Article::where('id', $id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'images' => $imageName
                // 'user_id' => Auth::user()->id
        ]);

        $edited = Article::where('id', $id)->first();
        DB::table('article_has_tags')->where('article_id', $id)->delete();
        DB::table('article_has_tags')->insert([
            'tag_id' => $tag->id,
            'article_id' => $edited->id
        ]);
        Alert::success('Success','Artikel Berhasil di Edit');

        return redirect('/project')->with('berhasil', 'Your Article has Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        DB::table('article_has_tags')->where('article_id', $id)->delete();
        Article::destroy($id);
        return redirect('/project')->with('berhasil', 'Your Article has Erased!');
    }


}
