<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;
use App\Tag;

class IndexController extends Controller
{
    public function index() {
        $articles = Article::all();
        // $articles = DB::table('article_has_tags')
        //         ->join('articles', 'articles.id', '=', 'article_has_tags.article_id')
        //         ->join('tags', 'tags.id', '=', 'article_has_tags.tag_id')
        //         ->select('articles.*', 'tags.tag_name')
        //         ->get();
        //dd($info);
        return view('index', compact('articles'));
    }

    public function show(Request $request, $id) {
        $article = Article::find($id);

        //untuk ambil tagsnya juga
        $info = DB::table('article_has_tags')
                ->join('articles', 'articles.id', '=', 'article_has_tags.article_id')
                ->join('tags', 'tags.id', '=', 'article_has_tags.tag_id')
                ->select('articles.*', 'tags.tag_name')
                ->where('articles.id', $id)
                ->get();
        // dd($info);


        return view('indexes.komenshow', compact('article', 'info'));
    }

    public function store(Request $request, Article $articles){
        // dd($articles);
    }
}
