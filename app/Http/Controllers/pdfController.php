<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use PDF;

class pdfController extends Controller
{
    public function peraturan(){

        $data = "Peraturan yang harus dipatuhi : 1. Dilarang mengupload artikel yang memiliki unsur SARA ;
        2. Dilarang Melecehkan pihak lain yaaa ; 3. Gunakan gambar yang sopan karena forum ini tidak hanya untuk yang dewasa";

        $pdf = PDF::loadView('pdf.peraturan', compact('data'));
        return $pdf->download('peraturan!.pdf');
    }
}
