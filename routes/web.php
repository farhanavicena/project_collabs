<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('/', 'IndexController');
Route::resource('/index', 'IndexController');

Route::get('/master', function () {
    return view('layouts.master');
});

Route::resource('project', 'ProjectController')->middleware('auth');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pdf-peraturan','pdfController@peraturan');
Route::get('/export-articles', 'ExcelController@export')->middleware('auth');
