@extends('layouts.master')

@section('content')
<div class="mt-3 ml-5 bg-light container" align="center" >
    <img src="{{asset('/img/article/'. $article->images)}}">
    <h2> {{ $article->judul }} </h2>
    <p> {{ $article->isi }} </p>
    <div style="padding-bottom: 30px">
        Tags :
        @foreach($article ->tags as $tag)
        <button Class="btn btn-primary btn-sm">{{$tag->tag_name}}</button>
        @endforeach
    </div>

</div>

<div class="mt-3 ml-5 bg-light container" align="center">
    <div class="panel-heading">Tambahkan komentar</div>
    <div class="panel-body">
        <form action="/" class="form-horizontal">
            @csrf

            {{csrf_field()}}
            <textarea rows="5" cols="30" class="form-control" name="isi_komentar"></textarea>
            <input style="margin: 30px"   type="submit" value="komentar" class="btn btn-primary">
        </form>
    </div>
</div>








@endsection
