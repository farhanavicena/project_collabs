@extends('layouts.master')

@section('content')

<div class="mt-3 ml-3">
			<div class="card">
              <div class="card-header bg-dark text-white">
                <h3 class="card-title">Articles Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              	@if(session('berhasil'))
              		<div class="alert alert-success">
              			{{ session('berhasil') }}
              		</div>
              	@endif
              	<a class="btn btn-primary mb-3 " href="/project/create"> Create Article </a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Judul</th>
                      <th>Gambar</th>
                      <th>Isi</th>
                      <th>Hastag</th>
                      <th style="width: 40px">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($articles as $key => $projek)
	                    <tr>
	                    	<td> {{ $key + 1 }} </td>
	                    	<td> {{ $projek -> judul }} </td>
                                <td> <img src="{{asset('/img/article/'. $projek->images)}}" height="75"> </td>
	                    	<td> {{ $projek -> isi }} </td>
	                    	<td>
                            @foreach($projek->tags as $tagss)
                            <button Class="btn btn-primary btn-sm">{{$tagss->tag_name}}</button>
                            @endforeach
                            </td>
	                    	<td style="display: flex;">
	                    		<a href="/project/{{$projek->id}}" class="btn btn-info btn-sm" >show</a>
	                    		<a href="/project/{{$projek->id}}/edit" class="btn btn-default btn-sm" >edit</a>
	                    		<form action="/project/{{$projek->id}}" method="post">
	                    			@csrf
	                    			@method('DELETE')
	                    			<input type="submit" value="delete" class="btn btn-danger btn-sm">
	                    		</form>
	                    	</td>
	                    </tr>
                    @empty
	                    <tr>
	                    	<td colspan="4" align="center"> No Posts </td>
	                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
             <!--  <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>

</div>

















@endsection
