@extends('layouts.master')

@section('content')
<div class="ml-10 mr-10">  
    <div class="card card-primary">
        <div class="card-header bg-dark text-white">
            <h3 class="card-title" align="middle">Make Your Article!</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/project" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul Berita</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul','')}}" placeholder="Write Down the Title!">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="input-group">
                    <span class="input-group-text">Isi Berita</span>
                    <textarea class="form-control" rows="7" cols="20" id="isi" name="isi" value="{{old('isi','')}}" aria-label="With textarea" placeholder="What is Your Story?"></textarea>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="input-group mt-3">
                    <span class="input-group-text" id="basic-addon1">Tag Berita</span>
                    <input type="text" class="form-control" name="tag" placeholder="Put Your Tag!" aria-label="tag" value="{{old('tag','')}}" aria-describedby="basic-addon1">
                    @error('tag')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="input-group mt-3">
                    <span class="input-group-text">Gambar</span>
                    <input type="file" name="image" class="form-control">
                    @error('gambar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                </card-body>

                <div class="card-footer" align="right">
                    <button type="submit" class="btn btn-dark mt-5">Make it!</button>
                </div>
        </form>
    </div>
</div>

@endsection