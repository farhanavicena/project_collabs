@extends('layouts.master')

@section('content')
<div class="mt-3 ml-5 bg-light container" align="center" >
    <img src="{{asset('/img/article/'. $article->images)}}">
    <h2> {{ $article->judul }} </h2>
    <p> {{ $article->isi }} </p>
    <div style="padding-bottom: 30px">
        Tags :
        @foreach($article ->tags as $tag)
        <button Class="btn btn-primary btn-sm">{{$tag->tag_name}}</button>
        @endforeach
    </div>

</div>

@endsection
