@extends('layouts.master')

@section('content')


<div class="ml-4">
    <div class="card card-dark">
        <div class="card-header bg-dark text-white" align="center">
            <h3 class="card-title">Edit Your Article No. {{ $article->id }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/project/ {{$article->id}} " method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="title">Judul Berita</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $article->judul)}}" placeholder="Write Down the Title Again!">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="input-group">
                    <span class="input-group-text">Isi Berita</span>
                    <input class="form-control" rows="7" cols="20" id="isi" name="isi" value="{{old('isi', $article->isi)}}" aria-label="With textarea" placeholder="What is Your Story?"></textarea>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="input-group mt-3">
                    <span class="input-group-text" id="basic-addon1">Tag Berita</span>
                    <input type="text" class="form-control" name="tag" placeholder="Put Your Tag!" aria-label="tag" value="{{old('tag',$info[0]->tag_name)}}" aria-describedby="basic-addon1">
                    @error('tag')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>

                <img src="{{asset('/img/article/'. $article->images)}}">
                <div class="input-group mt-3">
                    <span class="input-group-text">Gambar</span>
                    <input type="file" name="image" class="form-control">
                    @error('gambar')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <!--  -->
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-dark">Change it!</button>
                </div>
        </form>
    </div>
</div>


@endsection
