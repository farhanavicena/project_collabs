
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Yoga Studio Template">
        <meta name="keywords" content="Yoga, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>NEWS IE - Your News Portal</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="{{asset('/css/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('/css/nice-select.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('/css/slicknav.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('/css/style.css')}}" type="text/css">
    </head>

    <body class="bg-dark">
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Header Section Begin -->
        <header class="header-section-other bg-white">

            <div class="container-fluid">

                <div class="logo">
                    <a href="/"><img src="img/news.png" alt="" height="60"></a>
                </div>
                <div class="nav-menu">
                    <nav class="main-menu mobile-menu">
                        <ul>
                            <li class="active"><a href="/">Beranda</a></li>
                            <li><a href="#">Halaman</a>
                                <ul class="sub-menu">
                                    <li><a href="">Kategori</a></li>
                                    <li><a href="/project">Artikel Anda</a></li>
                                    <li><a href="/pdf-peraturan">Peraturan Posting</a></li>
                                    <li><a href="/export-articles">Export Article</a></li>

                                </ul>
                            </li>
                            <li><a href="">Profil</a></li>
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li><a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form></li>
                                    </ul>

                                </li>
                            @endguest
                        </ul>
                    </nav>
                    <div class="nav-right search-switch">
                        <i class="fa fa-search"></i>
                    </div>
                </div>
                <div id="mobile-menu-wrap"></div>

            </div>

        </header>

        <!-- Blog Section Begin -->
        <section class="blog-section spad">
            @yield('content')

        </section>
        <!-- Blog Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer-section bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="fs-left">
                            <div class="logo">
                                <a href="/">
                                    <img src="img/news.png" alt="">
                                </a>
                            </div>
                            <p>Sebuah portal berita yang dibuat dari anda, untuk anda, dan oleh anda.
                            Ayo saling berbagi informasi-informasi unik dan menarik!</p>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <form action="#" class="subscribe-form">
                            <h3>Subscribe to our newsletter</h3>
                            <input type="email" placeholder="Your e-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                        <div class="social-links">
                            <a href="#"><i class="fa fa-instagram"></i><span>Instagram</span></a>
                            <a href="#"><i class="fa fa-pinterest"></i><span>Pinterest</span></a>
                            <a href="#"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                            <a href="#"><i class="fa fa-twitter"></i><span>Twitter</span></a>
                            <a href="#"><i class="fa fa-youtube"></i><span>Youtube</span></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright-text">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->

        <!-- Search model -->
        <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch">+</div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search model end -->

        <!-- Js Plugins -->
        <script src="{{asset('/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{asset('/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/jquery.slicknav.js')}}"></script>
        <script src="{{asset('/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{asset('/js/mixitup.min.js')}}"></script>
        <script src="{{asset('/js/main.js')}}"></script>

        @stack('scripts')
        @include ('sweetalert::alert')
    </body>

</html>
